// ==UserScript==
// @name         TFS
// @namespace    http://tampermonkey.net/
// @version      0.7
// @description  Add additional features to TFS
// @author       Edward Salter
// @match        https://tfs.briefyourmarket.com/BYM2020/*
// @icon         https://www.google.com/s2/favicons?domain=briefyourmarket.com
// @grant        none
// @updateURL    https://gitlab.com/mayhem366/userscripts/-/raw/main/TFS/main.js
// @downloadURL  https://gitlab.com/mayhem366/userscripts/-/raw/main/TFS/main.js
// @supportURL   https://gitlab.com/mayhem366/userscripts/-/issues
// ==/UserScript==

(function () {
  "use strict";

  const MENU_ID = "project-choice-menu";
  const PROJECTS = [
    "Audience",
    "Build",
    "Core",
    "Ecosystem",
    "The Monolith"
  ]; // TODO: Can we call the API somehow to get list dynamically?

  /** @type {HTMLElement} */
  let projectCrumb = document.querySelectorAll(".bolt-breadcrumb-item")[1];

  /** @type {HTMLElement | null} */
  let menu = null;

  function addStyles() {
    console.log("Adding styles");
    const styles = document.createElement("style");
    styles.innerText = `.popup-menu__root {
  position: fixed;
  z-index: 200;
  background: rgba(0,0,0,0.01);
}
  
.project-menu__list {
  background: var(--callout-background-color,rgba(255, 255, 255, 1));
  margin: 0;
  padding: 0.5em 0;
  min-width: 100px;
}

.project-menu__list-item {
    outline: transparent;
    position: relative;
    font-size: 14px;
    color: inherit;
    background-color: transparent;
    border: none;
    width: 100%;
    height: 32px;
    line-height: 32px;
    display: block;
    cursor: pointer;
    padding: 0px 6px;
    text-align: left;
}

body .ms-ContextualMenu-link:hover:not([disabled],.user-menu-item), body .ms-ContextualMenu-link.is-expanded, body .pinned-menu-item .ms-ContextualMenu-link:hover:not([disabled]) {
    background: rgba(0,0,0,.04);
    background: var(--palette-black-alpha-4,rgba(0, 0, 0, .04));
}

.bolt-link.active {
    background-color: rgba(0,0,0,.08);
    background-color: var(--palette-black-alpha-8,rgba(0, 0, 0, .08));
}
`;
    document.head.appendChild(styles);
  }

  function showMenu() {
    if (menu && menu.isConnected) return; // Do nothing if we already have a menu
    console.log("Showing project choice menu");
    const projectCrumbRect = projectCrumb.getBoundingClientRect();
    projectCrumb.classList.add("active");

    menu = document.createElement("div");
    menu.id = MENU_ID;
    menu.className = "popup-menu__root";
    menu.style.top = projectCrumbRect.top + "px";
    menu.style.left = projectCrumbRect.left + "px";
    menu.style.paddingTop = projectCrumbRect.height + "px";

    const listItems = PROJECTS.map((project) => {
      let paths = window.location.pathname.split("/");
      paths[2] = project; // Change the project
      paths = paths.slice(0, 4); // Ignore any path after the next URL segment.
      const newPath = paths.join("/");

      return `<li class="ms-ContextualMenu-item" >
  <a href="${newPath}" class="ms-ContextualMenu-link project-menu__list-item bolt-link">${project}</a>
  </li>`;
    }).join("\n");
    menu.innerHTML = `<ul class="project-menu__list bolt-callout-shadow">
    ${listItems}
</ul>`;
    menu.onmouseleave = closeMenu;

    document.body.appendChild(menu);
  }

  function closeMenu() {
    projectCrumb.classList.remove("active");
    if (!menu) return; // Do nothing if we have no menu
    console.log("Closing project choice menu");
    menu.remove();
    menu = null;
  }

  function attachListenerToCrumb(searchNode = document) {
    projectCrumb = searchNode.querySelector(
      '[aria-describedby$="-item-described-byProject"]'
    );

    if (!projectCrumb) {
      console.warn("Couldn't find project crumb. No menu has been attaced.");
      return;
    }
    projectCrumb.addEventListener("mouseenter", showMenu);
  }

  /**
   * Function that is called once the page has DOM nodes added/removed
   * @param {MutationRecord[]} mutationsList
   */
  function pageMutationCallback(mutationsList) {
    for (const mutation of mutationsList) {
      // Ignore removals only, we are only interested in additions.
      if (mutation.addedNodes.length === 0) continue;

      console.log("Page has changed. Re-attaching event handlers.");

      const crumbContainer = mutation.target.querySelector(
        ".bolt-breadcrumb-with-items"
      );
      const crumbContainerMutationObserver = new MutationObserver(
        crumbContainerMutationCallback
      );
      crumbContainerMutationObserver.observe(crumbContainer, {
        childList: true,
        subtree: true,
      });
    }
  }

  function crumbContainerMutationCallback(mutationsList) {
    for (const mutation of mutationsList) {
      if (mutation.addedNodes.length === 0) continue;
      console.log("Breadcrumbs have changed, re-attaching event handlers.");
      attachListenerToCrumb(mutation.addedNodes[0]);
    }
  }

  // Create an observer instance linked to the callback function
  const observer = new MutationObserver(pageMutationCallback);

  const pageContainer = document.querySelector('[data-componentregion="page"]');
  addStyles();
  attachListenerToCrumb();
  // Start observing the target node for configured mutations
  observer.observe(pageContainer, {
    attributes: false,
    childList: true,
    subtree: false,
  });
})();
